FROM golang:1.17-buster as build
WORKDIR /go/src/app
ADD . /go/src/app
RUN go get -d -v ./...
RUN go build -o /go/bin/main

FROM gcr.io/distroless/base-debian10
COPY --from=build /go/bin/main /
CMD ["/main"]
